# DenW Connector

Deze repository bevat de projectcode voor de DenW connector.

Als je vragen hebt, kan je contact opnemen met support, op www.purplepolarbear.nl

## Overige DenW componenten
De DenW templates kunnen gevonden worden op https://gitlab.com/fair-data-communities/denw/assets

De DenW evaluator kan gevonden worden op https://gitlab.com/fair-data-communities/denw/evaluator

## Support
Voor meer informatie, kan je contact opnemen met Purple Polar Bear: www.purplepolarbear.nl

## Roadmap
De connector is onderdeel van de Sido samenwerking.

## Contributie
Zie de contributing file: https://gitlab.com/fair-data-communities/denw/assets/-/blob/master/CONTRIBUTING

## Licentie
De licentie wordt op volgende locatie beschreven: https://gitlab.com/fair-data-communities/denw/assets/-/blob/master/LICENSE
