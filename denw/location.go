package denw

import(
  "encoding/xml"
  "fmt"
  "strconv"
)

//
// API
//

type Location struct {
  XMLName   xml.Name
  Id        string `xml:"gml:id,attr"`

  Provider  string `xml:"DenW:Provider"`
  Name      string `xml:"name"`
  X         float64 `xml:"x"`
  Y         float64 `xml:"y"`
}

func NewLocation(layer *LocationLayer) *Location {
  return &Location{
    Provider: layer.Provider(),
  }
}

func (asset *Location) XmlName() *xml.Name {
  return &asset.XMLName
}

func (asset *Location) SetValue(key string, value interface {}) {
  fmt.Printf("Setting value for: %s\n", key)
  switch key {
  case "id":
    asset.Id = makeId(value)
  case "localName":
    asset.Name = value.(string)
  case "locationArray":
    values := valueAsArray(value)
    asset.X = values[0].(float64)
    asset.Y = values[1].(float64)
  case "locationX":
    asset.X = value.(float64)
  case "locationY":
    asset.Y = value.(float64)
  default:
  }
}

func (asset *Location) GetValue(key string) interface {} {
  switch key {
  case "id":
    return asset.Id
  case "localName":
    return asset.Name
  case "locationX":
    return asset.X
  case "locationY":
    return asset.Y
  default:
    panic(fmt.Sprintf("Unknown field for GetValue: %s", key))
  }
}

//
// Internal
//

func makeId(value interface {}) string {
  switch value := value.(type) {
  case string:
    return value
  case float64:
    return strconv.FormatFloat(value, 'f', -1, 64)
  }

  // TODO: Add watchdog call
  return "Unknown-ID"
}

func valueAsArray(value interface {}) []interface{} {
  return value.([]interface{})
}
