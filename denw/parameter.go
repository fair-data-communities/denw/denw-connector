package denw

import(
  "encoding/xml"
  "fmt"
)

//
// API
//

type Parameter struct {
  XMLName   xml.Name
  Id        string `xml:"gml:id,attr"`

  Provider                string `xml:"DenW:Provider"`
  Name                    string `xml:"name"`
  LocalName               string `xml:"localname"`
  UnitOfMeasurement       string `xml:"unitOfMeasurement"`
  LocalUnitOfMeasurement  string `xml:"localUnitOfMeasurement"`
}

func NewParameter(layer *ParameterLayer) *Parameter {
  return &Parameter{
    Provider: layer.Provider(),
  }
}

func (asset *Parameter) XmlName() *xml.Name {
  return &asset.XMLName
}

func (asset *Parameter) SetValue(key string, value interface {}) {
  fmt.Printf("    Setting value for: %s\n", key)
  switch key {
  case "id":
    asset.Id = makeId(value)
  case "name":
    asset.Name = value.(string)
  case "localName":
    asset.LocalName = value.(string)
  case "unitOfMeasurement":
    asset.UnitOfMeasurement = value.(string)
  case "localUnitOfMeasurement":
    asset.LocalUnitOfMeasurement = value.(string)
  default:
  }
}

func (asset *Parameter) GetValue(key string) interface {} {
  switch key {
  case "id":
    return asset.Id
  case "name":
    return asset.Name
  case "localName":
    return asset.LocalName
  case "unitOfMeasurement":
    return asset.UnitOfMeasurement
  case "localUnitOfMeasurement":
    return asset.LocalUnitOfMeasurement
  default:
    panic(fmt.Sprintf("Unknown field for GetValue: %s", key))
  }
}

//
// Internal
//
