package denw

import(
  "strings"
  
  "purplepolarbear.nl/wfs_service/core"
  "purplepolarbear.nl/wfs_service/models"
)

//
// API
//

type LocationLayer struct {
  name                      string;
  provider                  string;
  getfeatureproviderfactory models.GetFeatureProviderFactory;
  featureconvertorfactory   models.FeatureConvertorFactory;
}

func NewLocationLayer(name string, provider string, getfeatureproviderfactory models.GetFeatureProviderFactory, featureconvertorfactory models.FeatureConvertorFactory) *LocationLayer {
  return &LocationLayer{
    name: name,
    provider: strings.ToLower(provider),
    getfeatureproviderfactory: getfeatureproviderfactory,
    featureconvertorfactory: featureconvertorfactory}
}

func (layer *LocationLayer) NewItem() models.LayerItem {
  return NewLocation(layer)
}

// Implementation of Name()
func (layer *LocationLayer) Name() string {
  return layer.name;
}

// Implementation of Provider()
func (layer *LocationLayer) Provider() string {
  return layer.provider;
}

func (layer *LocationLayer) Namespace() string {
  return "DenW";
}

// Implementation of LayerType()
func (layer *LocationLayer) LayerType() string {
  return "Location";
}

func (layer *LocationLayer) FullName() (string) {
  return layer.Provider() + " " + layer.Name()
}

func (layer *LocationLayer) FullNamespacedName() (string) {
  return layer.Namespace() + ":" + layer.Name()
}

func (layer *LocationLayer) FullTypeName() string {
  return core.Snakecase(layer.Provider() + layer.LayerType())
}

func (layer *LocationLayer) FullNamespacedTypeName() string {
  return core.Snakecase(layer.Namespace() + ":" + layer.Provider() + layer.LayerType())
}

func (layer *LocationLayer) LayerProperties() *[]*models.LayerProperty {
  return &[]*models.LayerProperty{
    &models.LayerProperty{Name: "id", Type: "xsd:string", Optional: false},
    &models.LayerProperty{Name: "name", Type: "xsd:string", Optional: true},
    &models.LayerProperty{Name: "x", Type: "xsd:string", Optional: false},
    &models.LayerProperty{Name: "y", Type: "xsd:string", Optional: false},
    }
}

func (layer *LocationLayer) GetFeatureProviderFactory() (models.GetFeatureProviderFactory) {
  return layer.getfeatureproviderfactory;
}

func (layer *LocationLayer) FeatureConvertorFactory() models.FeatureConvertorFactory {
  return layer.featureconvertorfactory;
}

//
// Internal
//
