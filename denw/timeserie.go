package denw

import(
  "encoding/xml"
  "fmt"
)

//
// API
//

type Timeserie struct {
  XMLName   xml.Name
  Id        string `xml:"gml:id,attr"`

  Provider                string `xml:"DenW:Provider"`
  LocatieId     string `xml:"locatieId"`
  ParameterId   string `xml:"parameterId"`
  DateTime      string `xml:"datetime"`
  Value         string `xml:"value"`
}

func NewTimeserie(layer *TimeserieLayer) *Timeserie {
  return &Timeserie{
    Provider: layer.Provider(),
  }
}

func (asset *Timeserie) XmlName() *xml.Name {
  return &asset.XMLName
}

func (asset *Timeserie) SetValue(key string, value interface {}) {
  fmt.Printf("    Setting value for: %s\n", key)
  switch key {
  case "id":
    asset.Id = makeId(value)
  case "locatieId":
    asset.LocatieId = value.(string)
  case "parameterId":
    asset.ParameterId = value.(string)
  case "dateTime":
    asset.DateTime = value.(string)
  case "value":
    asset.Value = value.(string)
  default:
  }
}

func (asset *Timeserie) GetValue(key string) interface {} {
  switch key {
  case "id":
    return asset.Id
  case "locatieId":
    return asset.LocatieId
  case "parameterId":
    return asset.ParameterId
  case "dateTime":
    return asset.DateTime
  case "value":
    return asset.Value
  default:
    panic(fmt.Sprintf("Unknown field for GetValue: %s", key))
  }
}

//
// Internal
//
