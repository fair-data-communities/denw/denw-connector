package denw

import(
  "strings"

  "purplepolarbear.nl/wfs_service/core"
  "purplepolarbear.nl/wfs_service/models"
)

//
// API
//

type ParameterLayer struct {
  name                      string;
  provider                  string;
  getfeatureproviderfactory models.GetFeatureProviderFactory;
  featureconvertorfactory   models.FeatureConvertorFactory;
}

func NewParameterLayer(name string, provider string, getfeatureproviderfactory models.GetFeatureProviderFactory, featureconvertorfactory models.FeatureConvertorFactory) *ParameterLayer {
  return &ParameterLayer{
    name: name,
    provider: strings.ToLower(provider),
    getfeatureproviderfactory: getfeatureproviderfactory,
    featureconvertorfactory: featureconvertorfactory}
}

func (layer *ParameterLayer) NewItem() models.LayerItem {
  return NewParameter(layer)
}

// Implementation of Name()
func (layer *ParameterLayer) Name() string {
  return layer.name;
}

// Implementation of Provider()
func (layer *ParameterLayer) Provider() string {
  return layer.provider;
}

func (layer *ParameterLayer) Namespace() string {
  return "DenW";
}

// Implementation of LayerType()
func (layer *ParameterLayer) LayerType() string {
  return "Parameter";
}

func (layer *ParameterLayer) FullName() (string) {
  return layer.Provider() + " " + layer.Name()
}

func (layer *ParameterLayer) FullNamespacedName() (string) {
  return layer.Namespace() + ":" + layer.Name()
}

func (layer *ParameterLayer) FullTypeName() string {
  return core.Snakecase(layer.Provider() + layer.LayerType())
}

func (layer *ParameterLayer) FullNamespacedTypeName() string {
  return core.Snakecase(layer.Namespace() + ":" + layer.Provider() + layer.LayerType())
}

func (layer *ParameterLayer) LayerProperties() *[]*models.LayerProperty {
  return &[]*models.LayerProperty{
    &models.LayerProperty{Name: "id", Type: "xsd:string", Optional: false},
    &models.LayerProperty{Name: "name", Type: "xsd:string", Optional: true},
    &models.LayerProperty{Name: "x", Type: "xsd:string", Optional: false},
    &models.LayerProperty{Name: "y", Type: "xsd:string", Optional: false},
    }
}

func (layer *ParameterLayer) GetFeatureProviderFactory() (models.GetFeatureProviderFactory) {
  return layer.getfeatureproviderfactory;
}

func (layer *ParameterLayer) FeatureConvertorFactory() models.FeatureConvertorFactory {
  return layer.featureconvertorfactory;
}

//
// Internal
//
