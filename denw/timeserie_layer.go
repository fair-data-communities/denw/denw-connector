package denw

import(
  "strings"

  "purplepolarbear.nl/wfs_service/core"
  "purplepolarbear.nl/wfs_service/models"
)

//
// API
//

type TimeserieLayer struct {
  name                      string;
  provider                  string;
  getfeatureproviderfactory models.GetFeatureProviderFactory;
  featureconvertorfactory   models.FeatureConvertorFactory;
}

func NewTimeserieLayer(name string, provider string, getfeatureproviderfactory models.GetFeatureProviderFactory, featureconvertorfactory models.FeatureConvertorFactory) *TimeserieLayer {
  return &TimeserieLayer{
    name: name,
    provider: strings.ToLower(provider),
    getfeatureproviderfactory: getfeatureproviderfactory,
    featureconvertorfactory: featureconvertorfactory}
}

func (layer *TimeserieLayer) NewItem() models.LayerItem {
  return NewTimeserie(layer)
}

// Implementation of Name()
func (layer *TimeserieLayer) Name() string {
  return layer.name;
}

// Implementation of Provider()
func (layer *TimeserieLayer) Provider() string {
  return layer.provider;
}

func (layer *TimeserieLayer) Namespace() string {
  return "DenW";
}

// Implementation of LayerType()
func (layer *TimeserieLayer) LayerType() string {
  return "Timeserie";
}

func (layer *TimeserieLayer) FullName() (string) {
  return layer.Provider() + " " + layer.Name()
}

func (layer *TimeserieLayer) FullNamespacedName() (string) {
  return layer.Namespace() + ":" + layer.Name()
}

func (layer *TimeserieLayer) FullTypeName() string {
  return core.Snakecase(layer.Provider() + layer.LayerType())
}

func (layer *TimeserieLayer) FullNamespacedTypeName() string {
  return core.Snakecase(layer.Namespace() + ":" + layer.Provider() + layer.LayerType())
}

func (layer *TimeserieLayer) LayerProperties() *[]*models.LayerProperty {
  return &[]*models.LayerProperty{
    &models.LayerProperty{Name: "id", Type: "xsd:string", Optional: false},
    &models.LayerProperty{Name: "name", Type: "xsd:string", Optional: true},
    &models.LayerProperty{Name: "x", Type: "xsd:string", Optional: false},
    &models.LayerProperty{Name: "y", Type: "xsd:string", Optional: false},
    }
}

func (layer *TimeserieLayer) GetFeatureProviderFactory() (models.GetFeatureProviderFactory) {
  return layer.getfeatureproviderfactory;
}

func (layer *TimeserieLayer) FeatureConvertorFactory() models.FeatureConvertorFactory {
  return layer.featureconvertorfactory;
}

//
// Internal
//
